$(document).ready(function(){

	$('#btn_graph').click(function(){
		var value = $('#select_actions').val();

		clean_canvas();
		reset_tables();

		if(value == 'binary_trees'){
			draw_binary_tree($('#txt_graph_json').val());
		}else{
			graph = JSON.parse($('#txt_graph_json').val());

			draw_graph();  
			draw_adjacency_matrix(); 
			draw_adjacency_list();
			draw_edge_list();
		}
	});

	$('#btn_clean').click(function(){
		$('#txt_graph_json').val('');
	});

});

function change_state(){
	var value = $('#select_actions').val();

	if(value == 'binary_trees'){
		$('#lbl_textarea').text('Números:');	
	}else{
		$('#lbl_textarea').text('Formato JSON:');
	}
}