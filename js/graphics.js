function clean_canvas(){
	var canvas = $('#graphic_canvas')[0];
   	canvas.width = canvas.width;
}

function draw_node(x, y, value){
	var canvas = $('#graphic_canvas').get(0);
	var context = canvas.getContext('2d');
	var radius = 20;

	context.beginPath();
	context.strokeStyle = '#55B2E1';
	context.fillStyle = "#55B2E1";
	context.arc(x, y, radius, 0, 2 * Math.PI, false);
	context.lineWidth = 5;
	
	context.stroke();
	context.fill();

	context.textAlign = 'center';
	context.textBaseline = 'middle';
	//ct2.textBaseline = 'middle';
	context.fillStyle = "#000";
	context.font = "20px Arial";
	context.fillText(value, x, y);
}

function draw_relationship(x1, y1, x2, y2){
	var canvas = $('#graphic_canvas').get(0);
	var context = canvas.getContext('2d');
	var angle = Math.atan2(y2 - y1, x2 - x1);

	context.beginPath();
	context.lineCap = "round";
	context.strokeStyle = '#000';
	context.moveTo(x1, y1);
	context.lineTo(x2, y2);
	context.lineTo(x2 - 10 * Math.cos(angle - Math.PI / 6), y2 - 10 * Math.sin(angle - Math.PI / 6));
	context.moveTo(x2, y2);
	context.lineTo(x2 - 10 * Math.cos(angle + Math.PI / 6), y2 - 10 * Math.sin(angle + Math.PI / 6));
	context.stroke();
}