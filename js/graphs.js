var graph;

function draw_graph(){
   $.each(graph.el, function(index, arc){
      var node1 = graph.vl[arc.u];
      var node2 = graph.vl[arc.v];
      draw_relationship(node1.x, node1.y, node2.x, node2.y);
   });

   $.each(graph.vl, function(index, node){
      draw_node(node.x, node.y, index);
   });
}

function draw_adjacency_matrix(){
   $('#title_adjacency_matrix').attr('colspan', get_size() + 1);

   var html = get_head_adjacency_matrix();
   
   for (var i = 0; i < get_size(); i++) {
      html += get_row_adjacency_matrix(i);   
   }

   $('#adjacency_matrix > tbody').html(html);
}

function get_head_adjacency_matrix(){
   var html = '<tr><td></td>';

   for (var i = 0; i < get_size(); i++) {
      html += '<td align="center"><p><strong>' + i + '</strong></p></td>';
   }

   html += '</tr>';

   return html;
}

function get_row_adjacency_matrix(index){
   var html = '<tr><td align="center"><p><strong>' + index + '</strong></p></td>';

   for (var i = 0; i < get_size(); i++) {
      var adjacency_false = 'class="adjacency_false"';
      var adjacency = 0;

      $.each(graph.el, function(j, arc){
         if((arc.u == index && arc.v == i) ||
            (arc.u == i && arc.v == index)){
            adjacency_false = '';
            adjacency = 1;
         }
      });

      html += '<td align="center" ' + adjacency_false + '><p>' + adjacency + '</p></td>';
   }

   html += '</tr>';

   return html;
}

function draw_adjacency_list(){
   var html;
   var higher = 0;

   for (var i = 0; i < get_size(); i++) {
      var adjacency_list = get_adjacency_list(i);
      
      if(adjacency_list.length > higher){
         higher = adjacency_list.length;
      }

      html += '<tr>';
      html += '<td align="center"><p><strong>' + i + ':</strong></p></td>';

      for (var j = 0; j < adjacency_list.length; j++) {
         html += '<td align="center"><p>' + adjacency_list[j] + '</p></td>';      
      }
      
      html += '</tr>';
   }

   $('#title_adjacency_list').attr('colspan', higher + 1);
   $('#adjacency_list > tbody').html(html);
}

function get_adjacency_list(index){
   var adjacency_list = [];

   $.each(graph.el, function(i, arc){
      if(arc.u == index){
         adjacency_list.push(arc.v);
      } else if(arc.v == index){
         adjacency_list.push(arc.u);
      }
   });

   return adjacency_list.sort(function(a, b){
      return a-b
   });
}

function draw_edge_list(){
   var html = '';
   var checked_list = [];
   var count = 0;

   for (var i = 0; i < get_size(); i++) {
      var adjacency_list = get_adjacency_list(i);

      for (var j = 0; j < adjacency_list.length; j++) {
         if(!edge_is_checked(i, adjacency_list[j], checked_list)){
            html += '<tr>';
            html += '<td align="center"><p><strong>' + count + ':</strong></p></td>';
            html += '<td align="center"><p>' + i + '</p></td>';
            html += '<td align="center"><p>' + adjacency_list[j] + '</p></td>';      
            html += '</tr>';
            count++;
         }

         checked_list.push({'u': i, 'v': adjacency_list[j]});
      }

   }

   $('#title_edge_list').attr('colspan', 3);
   $('#edge_list > tbody').html(html);
}

function edge_is_checked(u, v, checked_list){
   var checked = false;

   for (var i = 0; i < checked_list.length; i++) {
      if((checked_list[i].u == u && checked_list[i].v == v)||
         (checked_list[i].u == v && checked_list[i].v == u)){
         checked = true;
      }
   }

   return checked;
}

function reset_tables(){
   $('#adjacency_matrix > tbody').html('');
   $('#adjacency_list > tbody').html('');
   $('#edge_list > tbody').html('');
}

function get_size(){
   if(graph){
      return Object.keys(graph.vl).length;
   }
}