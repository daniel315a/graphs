var tree;
var array_values;

function draw_binary_tree(values){
	tree = null;
	array_values = JSON.parse('[' + values + ']');

	for (var i = 0; i < array_values.length; i++) {
		add(array_values[i]);
	}

	draw_binary_tree_relationships(tree);
	draw_binary_tree_nodes(tree);
}

function draw_binary_tree_relationships(current){
	if(current != null){

		if(current.left != null){
			draw_relationship(current.x, current.y, current.left.x, current.left.y);
			draw_binary_tree_relationships(current.left);
		}
		//draw_node(current.x, current.y, current.value);
		if(current.right != null){
			draw_relationship(current.x, current.y, current.right.x, current.right.y);
			draw_binary_tree_relationships(current.right);
		}

	}
}

function draw_binary_tree_nodes(current){
	if(current != null){
		draw_node(current.x, current.y, current.value);
		draw_binary_tree_nodes(current.left);
		draw_binary_tree_nodes(current.right);
	}
}

function add(value){
	if(tree == null){
		tree = {
			'value':  value,
			'x': 320,
			'y': 30,
			'left': null,
			'right': null
		};
	}else{
		add_node(value, tree);
	}
}

function add_node(value, current){
	if(value < current.value){
		if(current.left == null){
			current.left = {
								'value': value,
								'x': current.x - 60,
								'y': current.y + 70,
								'left': null,
								'right': null
							}
		} else {
			add_node(value, current.left);
		}
	} else {
		if(current.right == null){
			current.right = {
								'value': value,
								'x': current.x + 60,
								'y': current.y + 70,
								'left': null,
								'right': null
							}
		} else {
			add_node(value, current.right);
		}
	}
}