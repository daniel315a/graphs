$(document).ready(function(){

   $('#btn_graph').click(function(){
      graph = JSON.parse($('#txt_graph_json').val());

      draw_graph();  
      draw_adjacency_matrix(); 
      draw_adjacency_list();
      draw_edge_list();
   });

});